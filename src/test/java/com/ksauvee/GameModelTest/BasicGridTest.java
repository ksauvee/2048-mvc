
package com.ksauvee.GameModelTest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import com.ksauvee.GameModel.BasicGrid;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class BasicGridTest {
    BasicGrid grid;

    @BeforeEach
    
    void initGrid() {
        grid = new BasicGrid();
    }

    @Test
    
    void gridDimensionsShouldBe4x4() {
        int[][] gridState = grid.getGridState();

        assertEquals(4, gridState.length);

        for (int[] row: gridState) {
            assertEquals(4, row.length);
        }
    }

    @Test
    
    void checkGridInitialization() {
        int[][] gridState = grid.getGridState();

        int squareTwoValues = 0;

        for (int[] row: gridState) {
            for (int squareValue: row) {
                if (squareValue == 2) {
                    squareTwoValues++;
                }
            }
        }

        assertEquals(2, squareTwoValues);
    }

    @Test
    
    void checkNoEndGame() {
        int[][] noEndGameGridState = {{2, 4, 8, 4}, {2, 8, 16, 2}, {64, 2, 0, 0}, {32, 128, 256, 512}};
        grid.setGridState(noEndGameGridState);

        assertFalse(grid.isEndGame());
    }

    @Test
    
    void checkEndGame() {
        int[][] endGameGridState = {{2, 4, 8, 4}, {4, 8, 16, 2}, {64, 2, 32, 8}, {32, 128, 256, 512}};
        grid.setGridState(endGameGridState);

        assertTrue(grid.isEndGame());
    }

    @Test
    
    void testLeftMoveWithOneFilledSquare() {
        int[][] gridState = {{0, 0, 0, 0}, {0, 0, 2, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}};
        grid.setGridState(gridState);
        grid.move('l');

        assertEquals(2, grid.getGridState()[1][0]);
    }

    @Test
    
    void testRightMoveWithOneFilledSquare() {
        int[][] gridState = {{0, 0, 0, 0}, {0, 0, 0, 0}, {4, 0, 0, 0}, {0, 0, 0, 0}};
        grid.setGridState(gridState);
        grid.move('r');

        assertEquals(4, grid.getGridState()[2][3]);
    }

    @Test
    
    void testUpMoveWithOneFilledSquare() {
        int[][] gridState = {{0, 0, 0, 0}, {0, 0, 512, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}};
        grid.setGridState(gridState);
        grid.move('u');

        assertEquals(512, grid.getGridState()[0][2]);
    }

    @Test
    
    void testDownMoveWithOneFilledSquare() {
        int[][] gridState = {{0, 0, 0, 0}, {0, 256, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}};
        grid.setGridState(gridState);
        grid.move('d');

        assertEquals(256, grid.getGridState()[3][1]);
    }

    @Test
    
    void testLeftMoveWithTwoEqualsValuesInARow() {
        int[][] gridState = {{0, 0, 0, 0}, {0, 256, 256, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}};
        grid.setGridState(gridState);
        grid.move('l');

        assertEquals(512, grid.getGridState()[1][0]);
    }

    @Test
    
    void testRightMoveWithTwoEqualsValuesInARow() {
        int[][] gridState = {{0, 0, 0, 0}, {0, 256, 256, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}};
        grid.setGridState(gridState);
        grid.move('r');

        assertEquals(512, grid.getGridState()[1][3]);
    }

    @Test
    
    void testUpMoveWithTwoEqualsValuesInAColumn() {
        int[][] gridState = {{8, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {8, 0, 0, 0}};
        grid.setGridState(gridState);
        grid.move('u');

        assertEquals(16, grid.getGridState()[0][0]);
    }

    @Test
    
    void testDownMoveWithTwoEqualsValuesInAColumn() {
        int[][] gridState = {{8, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {8, 0, 0, 0}};
        grid.setGridState(gridState);
        grid.move('d');

        assertEquals(16, grid.getGridState()[3][0]);
    }

    @Test
    
    void testLeftMoveWithTwoDifferentValuesInARow() {
        int[][] gridState = {{0, 0, 0, 0}, {0, 256, 512, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}};
        grid.setGridState(gridState);
        grid.move('l');

        assertEquals(256, grid.getGridState()[1][0]);
        assertEquals(512, grid.getGridState()[1][1]);
    }

    @Test
    
    void testRightMoveWithTwoDifferentValuesInARow() {
        int[][] gridState = {{0, 0, 0, 0}, {0, 256, 512, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}};
        grid.setGridState(gridState);
        grid.move('r');

        assertEquals(512, grid.getGridState()[1][3]);
        assertEquals(256, grid.getGridState()[1][2]);
    }

    @Test
    
    void testUpMoveWithTwoDifferentValuesInAColumn() {
        int[][] gridState = {{8, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {16, 0, 0, 0}};
        grid.setGridState(gridState);
        grid.move('u');

        assertEquals(8, grid.getGridState()[0][0]);
        assertEquals(16, grid.getGridState()[1][0]);
    }

    @Test
    
    void testDownMoveWithTwoDifferentValuesInAColumn() {
        int[][] gridState = {{8, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {16, 0, 0, 0}};
        grid.setGridState(gridState);
        grid.move('d');

        assertEquals(16, grid.getGridState()[3][0]);
        assertEquals(8, grid.getGridState()[2][0]);
    }

    @Test
    
    void testLeftMoveWithThreeEqualsValuesInARow() {
        int[][] gridState = {{0, 0, 0, 0}, {0, 256, 256, 256}, {0, 0, 0, 0}, {0, 0, 0, 0}};
        grid.setGridState(gridState);
        grid.move('l');

        assertEquals(512, grid.getGridState()[1][0]);
        assertEquals(256, grid.getGridState()[1][1]);
    }

    @Test
    
    void testRightMoveWithThreeEqualsValuesInARow() {
        int[][] gridState = {{0, 0, 0, 0}, {0, 256, 256, 256}, {0, 0, 0, 0}, {0, 0, 0, 0}};
        grid.setGridState(gridState);
        grid.move('r');

        assertEquals(512, grid.getGridState()[1][3]);
        assertEquals(256, grid.getGridState()[1][2]);
    }

    @Test
    
    void testUpMoveWithThreeEqualsValuesInAColumn() {
        int[][] gridState = {{8, 0, 0, 0}, {0, 0, 0, 0}, {8, 0, 0, 0}, {8, 0, 0, 0}};
        grid.setGridState(gridState);
        grid.move('u');

        assertEquals(16, grid.getGridState()[0][0]);
        assertEquals(8, grid.getGridState()[1][0]);
    }

    @Test
    
    void testDownMoveWithThreeEqualsValuesInAColumn() {
        int[][] gridState = {{8, 0, 0, 0}, {0, 0, 0, 0}, {8, 0, 0, 0}, {8, 0, 0, 0}};
        grid.setGridState(gridState);
        grid.move('d');

        assertEquals(16, grid.getGridState()[3][0]);
        assertEquals(8, grid.getGridState()[2][0]);
    }

    @Test
    
    void testLeftMoveCantDoMultipleFusions() {
        int[][] gridState = {{0, 0, 0, 0}, {0, 512, 256, 256}, {0, 0, 0, 0}, {0, 0, 0, 0}};
        grid.setGridState(gridState);
        grid.move('l');

        assertEquals(512, grid.getGridState()[1][0]);
        assertEquals(512, grid.getGridState()[1][1]);
    }

    @Test
    
    void testRightMoveCantDoMultipleFusions() {
        int[][] gridState = {{0, 0, 0, 0}, {0, 256, 256, 512}, {0, 0, 0, 0}, {0, 0, 0, 0}};
        grid.setGridState(gridState);
        grid.move('r');

        assertEquals(512, grid.getGridState()[1][3]);
        assertEquals(512, grid.getGridState()[1][2]);
    }

    @Test
    
    void testUpMoveCantDoMultipleFusions() {
        int[][] gridState = {{8, 0, 0, 0}, {0, 0, 0, 0}, {8, 0, 0, 0}, {16, 0, 0, 0}};
        grid.setGridState(gridState);
        grid.move('u');

        assertEquals(16, grid.getGridState()[0][0]);
        assertEquals(16, grid.getGridState()[1][0]);
    }

    @Test
    
    void testDownMoveCantDoMultipleFusions() {
        int[][] gridState = {{8, 0, 0, 0}, {0, 0, 0, 0}, {8, 0, 0, 0}, {16, 0, 0, 0}};
        grid.setGridState(gridState);
        grid.move('d');

        assertEquals(16, grid.getGridState()[3][0]);
        assertEquals(16, grid.getGridState()[2][0]);
    }

    @Test
    
    void newSquareShouldAppearAfterMove() {
        int[][] gridState = {{8, 0, 0, 0}, {0, 0, 0, 0}, {8, 0, 0, 0}, {16, 0, 0, 0}};
        grid.setGridState(gridState);
        grid.move('d');
        boolean isThereNewSquare = false;

        for (int[] row : this.grid.getGridState()) {
            for (int square : row) {
                if (square == 2) {
                    isThereNewSquare = true;
                    break;
                }
            }
        }

        assertTrue(isThereNewSquare);
    }
}
