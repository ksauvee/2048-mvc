package com.ksauvee;

import com.ksauvee.GameController.AbstractGameController;
import com.ksauvee.GameController.BasicGridController;
import com.ksauvee.GameModel.AbstractGameModel;
import com.ksauvee.GameModel.BasicGrid;
import com.ksauvee.GameView.BasicGridView;

public class App {
    public static void main(String[] args) {
        AbstractGameModel grid = new BasicGrid();
        AbstractGameController controller = new BasicGridController(grid);
        BasicGridView view = new BasicGridView(controller);
        grid.addObserver(view);
        view.update(grid.getGridState());
    }
}
