package com.ksauvee.Observer;

public interface Observer {
    public void update(int[][] data);
}
