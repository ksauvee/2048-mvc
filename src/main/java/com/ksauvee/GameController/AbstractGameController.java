package com.ksauvee.GameController;

import java.util.ArrayList;

import com.ksauvee.GameModel.AbstractGameModel;

public abstract class AbstractGameController {
    /**
     * Game model.
     */
    protected AbstractGameModel grid;

    /**
     * User move.
     */
    protected char move;

    /**
     * List of possible moves.
     */
    protected ArrayList<Character> possibleMoves;

    protected AbstractGameController(AbstractGameModel grid) {
        this.grid = grid;

        this.possibleMoves = new ArrayList<>();

        this.possibleMoves.add('l');
        this.possibleMoves.add('r');
        this.possibleMoves.add('u');
        this.possibleMoves.add('d');
    }

    public void setMove(char keyChar) {
        if (keyChar == 'q') {
            move = 'l';
        } else if (keyChar == 'd') {
            move = 'r';
        } else if (keyChar == 'z') {
            move = 'u';
        } else if (keyChar == 's') {
            move = 'd';
        }
        control();
    }

    public abstract void control();
}
