package com.ksauvee.GameController;

import com.ksauvee.GameModel.AbstractGameModel;

public class BasicGridController extends AbstractGameController {

    public BasicGridController(AbstractGameModel grid) {
        super(grid);
    }

    @Override
    public void control() {
        if (possibleMoves.contains(move)) {
            grid.move(move);
        }
        
        move = 0;
    }
}
