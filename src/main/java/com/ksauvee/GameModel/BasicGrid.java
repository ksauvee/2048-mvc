package com.ksauvee.GameModel;

import java.util.concurrent.ThreadLocalRandom;

/**
 * BasicGrid class represents Grid game 4x4 and basic initialization.
 */
public class BasicGrid extends AbstractGameModel {
    /**
     * BasicGrid constructor initialize Grid game 4x4 with 2 cells whose values = 2.
     */
    public BasicGrid() {
        ROW = 4;
        COLUMN = 4;

        gridState = new int[ROW][COLUMN];

        // initialize the BasicGrid with 2 cells whose values = 2
        for (int nbRow = 0; nbRow < ROW; nbRow++) {
            for (int nbColumn = 0; nbColumn < COLUMN; nbColumn++) {
                if (nbRow == 1 && nbColumn == 1 || nbRow == 3 && nbColumn == 2) {
                    gridState[nbRow][nbColumn] = 2;
                } else {
                    gridState[nbRow][nbColumn] = 0;
                }
            }
        }

        fusions = new boolean[ROW][COLUMN];

        for (int nbRow = 0; nbRow < ROW; nbRow++) {
            for (int nbColumn = 0; nbColumn < COLUMN; nbColumn++) {
                fusions[nbRow][nbColumn] = false;
            }
        }
    }

    /**
     * Gets current BasicGrid game.
     * @return a copy of the current BasicGrid game.
     */
    public int[][] getGridState() {
        int[][] newGridState = new int[ROW][COLUMN];
        // we use System.arraycopy in order to respect encapsulation
        for (int i = 0; i < ROW; i++) {
            System.arraycopy(gridState[i], 0, newGridState[i], 0, COLUMN);
        }
        return newGridState;
    }

    /**
     * Sets current BasicGrid game.
     * @param newGridState the new BasicGrid
     */
    public void setGridState(int[][] newGridState) {
        for (int i = 0; i < ROW; i++) {
            System.arraycopy(newGridState[i], 0, gridState[i], 0, ROW);
        }
    }

    /**
     * Check if current BasicGrid game is in end game state.
     * @return If end game Then true Else false
     */
    public boolean isEndGame() {
        for (int nbRow = 0; nbRow < ROW; nbRow++) {
            for (int nbColumn = 0; nbColumn < COLUMN; nbColumn++) {
                /* if one of the neighboring squares value are equals 0 or current square value
                then we can move the current square so there is no end game */
                if (nbRow > 0 && (gridState[nbRow - 1][nbColumn] == gridState[nbRow][nbColumn]
                || gridState[nbRow - 1][nbColumn] == 0)) {
                    return false;
                }

                if (nbRow < 3 && (gridState[nbRow + 1][nbColumn] == gridState[nbRow][nbColumn]
                        || gridState[nbRow + 1][nbColumn] == 0)) {
                    return false;
                }

                if (nbColumn > 0 && (gridState[nbRow][nbColumn - 1] == gridState[nbRow][nbColumn]
                        || gridState[nbRow][nbColumn - 1] == 0)) {
                    return false;
                }

                if (nbColumn < 3 && (gridState[nbRow][nbColumn + 1] == gridState[nbRow][nbColumn]
                        || gridState[nbRow][nbColumn + 1] == 0)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Apply the direction of the user input.
     * todo: refactor moves so that is more readable
     * @param userDirectionInput the user direction input.
     */
    public void move(final char userDirectionInput) {
        int[][] currentGridState = getGridState();
        if (userDirectionInput == 'l') {
            for (int nbRow = 0; nbRow < ROW; nbRow++) {
                for (int nbColumn = 0; nbColumn < COLUMN; nbColumn++) {
                    int currentColumn = 0;

                    while (gridState[nbRow][currentColumn] > 0 && gridState[nbRow][currentColumn] != gridState[nbRow][nbColumn]) {
                        currentColumn++;
                    }

                    if (nbColumn != currentColumn) {
                        gridState[nbRow][currentColumn] += gridState[nbRow][nbColumn];
                        gridState[nbRow][nbColumn] = 0;
                    }
                }
            }
        }

        else if (userDirectionInput == 'r') {
            for (int nbRow = 0; nbRow < ROW; nbRow++) {
                for (int nbColumn = COLUMN - 2; nbColumn >= 0; nbColumn--) {
                    int currentColumn = COLUMN - 1;

                    while (currentColumn > nbColumn && (gridState[nbRow][currentColumn] > 0 && gridState[nbRow][currentColumn] != gridState[nbRow][nbColumn])) {
                        currentColumn--;
                    }
                    
                    // if we go back to the current square we don't need to do anything
                    if (nbColumn != currentColumn) {
                        gridState[nbRow][currentColumn] += gridState[nbRow][nbColumn];
                        gridState[nbRow][nbColumn] = 0; 
                    }
                }
            }
        }

        else if (userDirectionInput == 'u') {
            for (int nbRow = 0; nbRow < ROW; nbRow++) {
                for (int nbColumn = 0; nbColumn < COLUMN; nbColumn++) {
                    int currentRow = 0;

                    while (gridState[currentRow][nbColumn] > 0 && (gridState[currentRow][nbColumn] != gridState[nbRow][nbColumn] || fusions[currentRow][nbColumn])) {
                        currentRow++;
                    }
                    
                    // if we go back to the current square we don't need to do anything
                    if (nbRow != currentRow) {
                        gridState[currentRow][nbColumn] += gridState[nbRow][nbColumn];
                        gridState[nbRow][nbColumn] = 0;
                        fusions[currentRow][nbColumn] = true;
                    }
                }
            }
        }

        else if (userDirectionInput == 'd') {
            for (int nbRow = ROW - 1; nbRow >= 0; nbRow--) {
                for (int nbColumn = 0; nbColumn < COLUMN; nbColumn++) {
                    int currentRow = ROW - 1;

                    while (gridState[currentRow][nbColumn] > 0 && gridState[currentRow][nbColumn] != gridState[nbRow][nbColumn]) {
                        currentRow--;
                    }
                    
                    // if we go back to the current square we don't need to do anything
                    if (nbRow != currentRow) {
                        gridState[currentRow][nbColumn] += gridState[nbRow][nbColumn];
                        gridState[nbRow][nbColumn] = 0;
                    }
                }
            }
        }

        if (!isSameGrid(currentGridState)) {
            generateNewSquare();
        }

        notifyObserver(getGridState());
    }

    /**
     * Generate new square with value equals 2.
     */
    public void generateNewSquare() {
        int randomRow = ThreadLocalRandom.current().nextInt(0, 3 + 1);
        int randomColumn = ThreadLocalRandom.current().nextInt(0, 3 + 1);

        while (gridState[randomRow][randomColumn] > 0) {
            randomRow = ThreadLocalRandom.current().nextInt(0, 3 + 1);
            randomColumn = ThreadLocalRandom.current().nextInt(0, 3 + 1);
        }

        gridState[randomRow][randomColumn] = 2;
    }

    /**
     * 
     */
    public boolean isSameGrid(final int[][] otherGridState) {
        for (int i = 0; i < ROW; i++) {
            for (int j = 0; j < COLUMN; j++) {
                if (gridState[i][j] != otherGridState[i][j]) {
                    return false;
                }
            }
        }

        return true;
    }
}
