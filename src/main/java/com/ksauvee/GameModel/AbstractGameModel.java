package com.ksauvee.GameModel;

import java.util.ArrayList;

import com.ksauvee.Observer.Observable;
import com.ksauvee.Observer.Observer;

public abstract class AbstractGameModel implements Observable {
    /**
     * Number of rows in the 2048 grid.
     */
    protected static int ROW;

    /**
     * Number of columns in the 2048 grid.
     */
    protected static int COLUMN;

    /**
     * Game grid.
     */
    protected int[][] gridState;

    /**
     * Fusions made during a movement.
     */
    protected boolean[][] fusions;

    /**
     * List of Model Observers.
     */
    private ArrayList<Observer> listObserver;

    /**
     * AbstractGameModel constructor which initialize the Observer's list.
     */
    protected AbstractGameModel() {
        listObserver = new ArrayList<>();
    }

    /**
     * Gets current grid game.
     * @return a copy of the current grid game.
     */
    public abstract int[][] getGridState();

    /**
     * Sets current grid game.
     * @param newGridState the new grid
     */
    public abstract void setGridState(int[][] newGridState);

    /**
     * Check if current grid game is in end game state.
     * @return If end game Then true Else false.
     */
    public abstract boolean isEndGame();

    /**
     * Apply the direction of the user input.
     * @param userDirectionInput the user direction input.
     */
    public abstract void move(final char userDirectionInput);

    /**
     * Add an observer to Observer's list.
     * @param obs the observer added.
     */
    public void addObserver(Observer obs) {
        listObserver.add(obs);
    }

    /**
     * Reset Observer's list.
     */
    public void removeObserver() {
        listObserver = new ArrayList<>();
    }

    /**
     * Notify observers of a change.
     */
    public void notifyObserver(int[][] data) {
        for (Observer obs : listObserver) {
            obs.update(data);
        }
    }
}
