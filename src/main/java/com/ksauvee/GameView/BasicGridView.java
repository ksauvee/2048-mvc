package com.ksauvee.GameView;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.ksauvee.GameController.AbstractGameController;
import com.ksauvee.Observer.Observer;

public class BasicGridView extends JFrame implements Observer, KeyListener {
    private final JPanel container;
    private final AbstractGameController controller;
    private final JLabel[][] tabSquares;

    public BasicGridView(AbstractGameController controller) {
        container = new JPanel();
        tabSquares = new JLabel[4][4];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                tabSquares[i][j] = new JLabel();
            }
        }
        this.setSize(500, 460);
        this.setTitle("2048");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        initComponent();
        this.controller = controller;
        this.setContentPane(container);
        this.setVisible(true);
        this.addKeyListener(this);
    }

    public void initComponent() {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                JPanel square = new JPanel();
                square.setPreferredSize(new Dimension(100, 100));
                square.setBackground(Color.WHITE);
                square.setBorder(BorderFactory.createLineBorder(Color.GRAY));

                tabSquares[i][j].setText("0");
                tabSquares[i][j].setFont(new Font("Arial", Font.PLAIN, 20));
                square.add(tabSquares[i][j]);
                container.add(square);
            }
        }
        container.setBorder(BorderFactory.createLineBorder(Color.GRAY));
    }

    @Override
    public void update(int[][] data) {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                tabSquares[i][j].setText(String.valueOf(data[i][j]));
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // TODO Auto-generated method stub
        controller.setMove(e.getKeyChar());
    }

    @Override
    public void keyPressed(KeyEvent e) {
        // TODO Auto-generated method stub
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // TODO Auto-generated method stub
    }
}
