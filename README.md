# 2048-mvc

## Introduction

The 2048 Game with MVC architecture, Unit Testing and GUI.

## Dependencies

Maven

## How to use this project ?

### How to start 2048-mvc ?

1. Go to project folder
2. Run 'mvn package' in shell
3. Finally run JAR create in target directory

### How to get documentation generates by JavaDoc ?

Open index.html file in docs directory within your favorite browser.

## Notes

### Done

### Not Done

1. Implement 2048 game
2. Add GUI